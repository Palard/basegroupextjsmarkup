# ext-theme-bg/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    ext-theme-bg/sass/etc
    ext-theme-bg/sass/src
    ext-theme-bg/sass/var
